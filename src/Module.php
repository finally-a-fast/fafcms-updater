<?php

namespace fafcms\updater;

use fafcms\updater\commands\MigrateController;
use fafcms\updater\commands\UpdateController;

use Yii;
use yii\helpers\Console;
use yii\console\Application;
use yii\base\Module as BaseModule;
use yii\base\ErrorException;
use yii\base\BootstrapInterface;

class Module extends BaseModule implements BootstrapInterface
{
    /**
     * Path for storing Updates.
     *
     * @var string $path
     */
    public string $updatePath = '@console/updates';

    /**
     * Template file used for generating the Update class
     *
     * @var string $templateFile
     */
    public string $templateFile = '@fafcms/updater/views/createUpdateFile.php';

    /**
     * The Namespace used for the Update class template
     *
     * @var string $updateNamespace
     */
    public string $updateNamespace = 'console\updates';

    /**
     * The Namespaces to look for available updates
     *
     * @var array
     */
    public array $updateNamespaces = [];

    /**
     * Table name used for storing the update logs
     *
     * @var string $tableName
     */
    public string $tableName = 'update';

    public $beforeUpdate;
    public $afterUpdate;

    /**
     * {@inheritDoc}
     */
    public function init() : void
    {
        parent::init();
        Yii::configure($this, require __DIR__ .'/config/config.php');

        $this->updateNamespaces[] = $this->updateNamespace;
    }

    /**
     * {@inheritDoc}
     *
     * @param \yii\base\Application $app
     * @throws ErrorException
     */
    public function bootstrap($app)
    {
        if ((!$app instanceof Application)) {
            throw new ErrorException('Only available as console command');
        }

        if (!isset(Yii::$app->controllerMap['update'])) {
            Yii::$app->controllerMap['update'] = UpdateController::class;
        }

        Yii::$app->controllerMap['migrate']['class'] = MigrateController::class;
    }

    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        if (!$this->validateConfig()) {
            return false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Validates the config
     *
     * @return bool
     */
    protected function validateConfig() : bool
    {
        if ($this->beforeUpdate !== false && !is_callable($this->beforeUpdate)) {
            Yii::$app->controller->stdout("beforeUpdate must be callable\n", Console::FG_RED);
            return false;
        }

        if ($this->afterUpdate !== false && !is_callable($this->afterUpdate)) {
            Yii::$app->controller->stdout("afterUpdate must be callable\n", Console::FG_RED);
            return false;
        }

        return true;
    }
}
