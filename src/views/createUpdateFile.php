<?php
/**
 * Template file used for generating the Update class
 *
 * @var $className string the new Update class name without namespace
 * @var $namespace string the new Update class namespace
 */

echo "<?php\n";
?>
namespace <?= $namespace ?>;

use fafcms\updater\base\Update;

/**
* Handles the Update
*/
class <?= $className ?> extends Update
{
    /**
    * {@inheritdoc}
    */
    public function up(): bool
    {
        return true;
    }

    /**
    * {@inheritdoc}
    */
    public function down(): bool
    {
        return true;
    }
}
