<?php

namespace fafcms\updater\models;

use fafcms\updater\Module;

use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use Yii;
use DateTime;
use DateTimeZone;

/**
 * Class Update
 *
 * @package fafcms\updater\models
 *
 * @property int $id
 * @property string $version
 * @property string $applied_at
 */
class Update extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName() : string
    {
        return Module::getInstance()->tableName;
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestampBehavior' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['applied_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['applied_at'],
                ],
                'value' => (new DateTime('NOW', new DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s'),
            ],
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function rules(): array
    {
        return [
            [['version'], 'required'],
            [['id'], 'integer'],
            [['version'], 'string', 'max' => 180],
            [['applied_at'], 'safe'],
        ];
    }
}
