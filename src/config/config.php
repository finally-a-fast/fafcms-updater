<?php

use fafcms\updater\Module;

return [
    'beforeUpdate' => static function (Module $module, array $updates) : bool {
        return true;
    },
    'afterUpdate' => static function (Module $module, array $updates) {
        return true;
    },
];
