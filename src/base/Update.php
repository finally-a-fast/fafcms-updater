<?php

namespace fafcms\updater\base;

use fafcms\updater\commands\MigrateController;
use fafcms\updater\models\Update as UpdateModel;
use Yii;
use yii\base\Component;
use yii\base\ErrorException;
use yii\helpers\Console;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;

abstract class Update extends Component
{
    public ?string $name = null;
    public ?string $description = null;

    /**
     * @param string $up
     *
     * @return bool
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function run(string $up): bool
    {
        $time    = $this->beginCommand($this->description);
        $success = $up === 'up' ? $this->beforeUp() : $this->beforeDown();
        $this->endCommand($time);

        return $success;
    }

    /**
     * Do the Update
     *
     * @return bool
     */
    abstract public function up(): bool;

    /**
     * Undo the Update
     */
    abstract public function down(): bool;

    /**
     * Prepares for a command to be executed, and outputs to the console.
     *
     * @param string|null $description the description for the command, to be output to the console.
     *
     * @return float the time before the command is executed, for the time elapsed to be calculated.
     */
    protected function beginCommand(?string $description = null): float
    {
        if ($description) {
            Console::output("    > $description ...");
        }

        return microtime(true);
    }

    /**
     * Finalizes after the command has been executed, and outputs to the console the time elapsed.
     *
     * @param float $time the time before the command was executed.
     */
    protected function endCommand(float $time): void
    {
        Console::output("\ndone (time: " . sprintf('%.3f', microtime(true) - $time) . 's)');
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    protected function beforeUp(): bool
    {
        $model          = new UpdateModel();
        $model->version = $this->name;
        $transaction    = Yii::$app->db->beginTransaction();
        $success        = $model->save();

        if ($success && $this->up()) {
            $transaction->commit();

            return true;
        }

        return false;
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    protected function beforeDown(): bool
    {
        if (($model = UpdateModel::find()->where(['version' => $this->name])->one()) === null) {
            throw new NotFoundHttpException('Could not find the Update');
        }

        $transaction = Yii::$app->db->beginTransaction();
        $success     = $model->delete();

        if ($success && $this->down()) {
            $transaction->commit();
            return true;
        }

        return false;
    }

    /**
     * @param string $className
     * @param string $direction
     *
     * @return bool
     * @throws ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    protected function migrate(string $className, string $direction): bool
    {
        $migration = \Yii::createObject($className);

        if (!($migration instanceof Migration)) {
            throw new ErrorException('You can only use migrations of type "' . Migration::class . '" inside an update.');
        }

        try {
            Yii::$app->runAction('migrate/run', [$className, $direction, true]);
        } catch (\Exception $e)  {
            Console::renderColoredString(PHP_EOL . 'Error running migration: "' . $className . '".' . PHP_EOL . $e->getMessage(), Console::FG_RED);
            return false;
        }

        return true;
    }

    /**
     * @param string $className
     *
     * @return bool
     * @throws ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    protected function migrateUp(string $className): bool
    {
        return $this->migrate($className, MigrateController::DIRECTION_UP);
    }

    /**
     * @param string $className
     *
     * @return bool
     * @throws ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    protected function migrateDown(string $className): bool
    {
        return $this->migrate($className, MigrateController::DIRECTION_DOWN);
    }
}
