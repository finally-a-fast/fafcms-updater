<?php

namespace fafcms\updater\base;

use Yii;

abstract class Migration extends \yii\db\Migration
{
    /**
     * Builds and executes a SQL statement for creating a new index.
     *
     * @param string       $table   the table that the new index will be created for. The table name will be properly quoted by the method.
     * @param string|array $columns the column(s) that should be included in the index. If there are multiple columns, please separate them
     *                              by commas or use an array. Each column name will be properly quoted by the method. Quoting will be skipped for column names that
     *                              include a left parenthesis "(".
     * @param bool         $unique  whether to add UNIQUE constraint on the created index.
     */
    public function createIndexByColumns(string $table, $columns, bool $unique = false): void
    {
        $cleanTableName = str_replace(['{{%', '}}'], '', $table);
        $name = 'idx-' . $cleanTableName . '-' . (is_array($columns) ? implode('-', $columns) : $columns);
        $this->createIndex($name, $table, $columns, $unique);
    }

    /**
     * Builds and executes a SQL statement for dropping an index.
     *
     * @param string       $table   the table that the new index will be created for. The table name will be properly quoted by the method.
     * @param string|array $columns the column(s) that should be included in the index. If there are multiple columns, please separate them
     *                              by commas or use an array. Each column name will be properly quoted by the method. Quoting will be skipped for column names that
     *                              include a left parenthesis "(".
     */
    public function dropIndexByColumns(string $table, $columns): void
    {
        $cleanTableName = str_replace(['{{%', '}}'], '', $table);
        $name = 'idx-' . $cleanTableName . '-' . (is_array($columns) ? implode('-', $columns) : $columns);
        $this->dropIndex($name, $table);
    }

    /**
     * Builds a SQL statement for adding a foreign key constraint to an existing table.
     * The method will properly quote the table and column names.
     *
     * @param string       $table      the table that the foreign key constraint will be added to.
     * @param string|array $columns    the name of the column to that the constraint will be added on. If there are multiple columns, separate them with commas or use an array.
     * @param string       $refTable   the table that the foreign key references to.
     * @param string|array $refColumns the name of the column that the foreign key references to. If there are multiple columns, separate them with commas or use an array.
     * @param string|null  $delete     the ON DELETE option. Most DBMS support these options: RESTRICT, CASCADE, NO ACTION, SET DEFAULT, SET NULL
     * @param string|null  $update     the ON UPDATE option. Most DBMS support these options: RESTRICT, CASCADE, NO ACTION, SET DEFAULT, SET NULL
     */
    public function addForeignKeyByColumns(string $table, $columns, string $refTable, $refColumns, ?string $delete = null, ?string $update = null): void
    {
        $cleanTableName = str_replace(['{{%', '}}'], '', $table);
        $name = 'fk-' . $cleanTableName . '-' . (is_array($columns) ? implode('-', $columns) : $columns);
        $this->addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    /**
     * Builds a SQL statement for dropping a foreign key constraint.
     *
     * @param string       $table   the table that the foreign key constraint will be added to.
     * @param string|array $columns the name of the column to that the constraint will be added on. If there are multiple columns, separate them with commas or use an array.
     */
    public function dropForeignKeyByColumns(string $table, $columns): void
    {
        $cleanTableName = str_replace(['{{%', '}}'], '', $table);
        $name = 'fk-' . $cleanTableName . '-' . (is_array($columns) ? implode('-', $columns) : $columns);
        $this->dropForeignKey($name, $table);
    }

    /**
     * @return bool
     */
    public function safeUp(): bool
    {
        return true;
    }

    /**
     * @return bool|null
     */
    public function up(): ?bool
    {
        if (Yii::$app->db->transaction !== null && Yii::$app->db->transaction->getIsActive()) {
            return $this->safeUp();
        }

        return parent::up();
    }

    /**
     * @return bool
     */
    public function safeDown(): bool
    {
        return false;
    }

    /**
     * @return bool|null
     */
    public function down(): ?bool
    {
        if (Yii::$app->db->transaction !== null && Yii::$app->db->transaction->getIsActive()) {
            return $this->safeDown();
        }

        return parent::down();
    }
}
