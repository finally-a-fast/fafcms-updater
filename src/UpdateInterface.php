<?php


namespace fafcms\updater;

/**
 * Interface UpdateInterface
 *
 * @package fafcms\updater
 * @deprecated
 */
interface UpdateInterface
{
    public function up() : bool;

    public function down() : bool;
}
