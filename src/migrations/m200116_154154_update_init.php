<?php

use fafcms\updater\Module;

use yii\db\Migration;

/**
 * Class m200116_154154_update_init
 */
class m200116_154154_update_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = Module::getInstance()->tableName;
        $cleanTableName = str_replace(['{{%', '}}'], '', $tableName);

        $this->createTable($tableName, [
            'id'         => $this->primaryKey()->unsigned()->notNull(),
            'version'    => $this->string(180)->notNull(),
            'applied_at' => $this->dateTime()->notNull(),
        ]);

        $this->createIndex(
            'idx-' . $cleanTableName . '-version',
            $tableName,
            'version',
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Module::getInstance()->tableName);
    }
}
