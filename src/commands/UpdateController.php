<?php

namespace fafcms\updater\commands;


use fafcms\updater\Module;
use fafcms\updater\models\Update as UpdateModel;
use fafcms\updater\base\Update;

use Yii;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii\console\ExitCode;
use yii\console\Exception;
use yii\console\Controller;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;

/**
 * Class UpdateController
 *
 * @package fafcms\updater\commands
 *
 * @property-read array $newUpdates
 */
class UpdateController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public $defaultAction = 'up';

    /**
     * @var array list of namespaces containing the update classes.
     *
     * Update namespaces should be resolvable as a [path alias](guide:concept-aliases) if prefixed with `@`, e.g. if you specify
     * the namespace `app\updates`, the code `Yii::getAlias('@app/updates')` should be able to return
     * the file path to the directory this namespace refers to.
     * This corresponds with the [autoloading conventions](guide:concept-autoloading) of Yii.
     *
     * For example:
     *
     * ```php
     * [
     *     'app\updates',
     *     'some\extension\updates',
     * ]
     * ```
     *
     * @since 0.2.0
     */
    public array $updateNamespaces = [];

    /**
     * @var string|null the template file for generating new updates.
     * This can be either a path alias (e.g. "@app/updates/template.php")
     * or a file path.
     * @since 0.2.0
     */
    public ?string $templateFile = null;

    /**
     * {@inheritdoc}
     */
    public function options($actionID)
    {
        return array_merge(
            parent::options($actionID),
            ['compact'],
            in_array($actionID, ['up', 'down'], true) ? ['updateNamespaces'] : [],
            $actionID === 'create' ? ['templateFile'] : []
        );
    }

    /**
     * {@inheritdoc}
     */
    public function optionAliases()
    {
        return array_merge(parent::optionAliases(), [
            'n' => 'updateNamespaces',
            'f' => 'templateFile',
        ]);
    }

    /**
     * @var Module $module
     */
    public $module;

    /**
     * {@inheritDoc}
     */
    public function init() : void
    {
        parent::init();

        if (!($this->module instanceof Module)) {
            $this->module = Module::getInstance();
        }
    }

    public function beforeAction($action)
    {
        $result = parent::beforeAction($action);

        if (count($this->updateNamespaces) === 0) {
            $this->updateNamespaces = $this->module->updateNamespaces;
        }

        if ($this->templateFile === null) {
            $this->templateFile = $this->module->templateFile;
        }

        return $result;
    }

    /**
     * @param string $name
     * @return int
     * @throws Exception
     * @throws yii\base\Exception
     */
    public function actionCreate(string $name) : int
    {
        if (!preg_match('/^[\w\\\\]+$/', $name)) {
            throw new Exception('The update name should contain letters, digits, underscore and/or backslash characters only.');
        }

        $className = 'u' . gmdate('ymd_His') . '_' . $name;
        $filePath  = Yii::getAlias($this->module->updatePath);
        $file      = rtrim($filePath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $className . '.php';

        if ($this->confirm('Create new Update "' . $file . '"?')) {
            $content = $this->generateUpdateSourceCode([
                'className' => $className,
                'namespace' => $this->module->updateNamespace,
            ]);

            FileHelper::createDirectory($filePath);
            file_put_contents($file, $content, LOCK_EX);
            $this->stdout('New Update created successfully' . PHP_EOL, Console::FG_GREEN);
        }

        return ExitCode::OK;
    }

    /**
     * Updates the Application
     *
     * @param int         $limit the number of new updates to be applied. 0 means all new updates.
     *
     * @return int
     * @throws InvalidConfigException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUp(int $limit = 0): int
    {
        $updates = $this->getNewUpdates();
        $countAll = count($updates);

        if ($countAll === 0) {
            $this->stdout('No new updates found.' . PHP_EOL, Console::FG_GREEN);
            return ExitCode::OK;
        }

        if ($limit > 0) {
            $updates = array_slice($updates, 0, $limit);
        }

        $count = count($updates);
        $this->stdout($count . ($count === $countAll ? '' : ' out of ' . $countAll) . ' new ' . ($countAll === 1 ? 'update' : 'updates') . ' will be applied:' . PHP_EOL, Console::FG_YELLOW);

        // List updates
        foreach ($updates as $name) {
            $this->stdout("\t" . $name . PHP_EOL);
        }

        $this->stdout(PHP_EOL);
        $applied = 0;

        if ($this->confirm('Apply ' . ($count === 1 ? 'Update' : 'Updates') . '?')) {
            if (is_callable($this->module->beforeUpdate) && !call_user_func($this->module->beforeUpdate, $this->module, $updates)) {
                $this->stdout("Function beforeUpdate failed.\nIt must return true in order to continue.", Console::FG_RED);
                return ExitCode::UNSPECIFIED_ERROR;
            }

            foreach ($updates as $className) {
                $update = $this->initUpdate($className);

                if (!$update->run('up')) {
                    $this->stdout("\n$applied out of $count " . ($applied === 1 ? 'update' : 'updates') . " applied.\n", Console::FG_RED);
                    $this->stdout(PHP_EOL . 'The Update ' . $className . ' failed. Continuing canceled' . PHP_EOL, Console::FG_RED);
                    return ExitCode::UNSPECIFIED_ERROR;
                }

                $applied++;
            }

            $this->stdout("\n$applied " . ($applied === 1 ? 'update' : 'updates') . " applied.\n", Console::FG_GREEN);

            if (is_callable($this->module->afterUpdate) && !call_user_func($this->module->afterUpdate, $this->module, $updates)) {
                $this->stdout("Function afterUpdate failed.\nIt must return true in order to continue.", Console::FG_RED);
                return ExitCode::UNSPECIFIED_ERROR;
            }

            $this->stdout("\nUpdated successfully.\n", Console::FG_GREEN);
        }

        return ExitCode::OK;
    }

    /**
     * Downgrades the Application
     *
     * @param int|string  $limit the number of updates to be reverted or 'all' to revert all updates
     *
     * @return int
     * @throws Exception
     * @throws InvalidConfigException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDown($limit = 1): int
    {
        if ($limit === 'all') {
            $limit = -1;
        } else if ($limit < 1) {
            throw new Exception('The step argument must be greater than 0.');
        } else {
            $limit = (int)$limit;
        }

        $conditions = [];

        foreach ($this->updateNamespaces as $updateNamespace) {
            if ($updateNamespace === $this->module->updateNamespace) {
                $conditions[] = ['NOT LIKE', 'version', '\\'];
            } else {
                $updateNamespace = strtr($updateNamespace, [
                    '%' => '\%',
                    '_' => '\_',
                    '\\' => '\\\\',
                ]);

                $conditions[] = ['LIKE', 'version', $updateNamespace . '%', false];
            }
        }

        $models = UpdateModel::find()->orderBy('applied_at DESC')->where(['OR', ...$conditions])->limit($limit)->all();

        $count   = count($models);

        if ($count === 0) {
            $this->stdout("No updates has been done.\n", Console::FG_YELLOW);
            return ExitCode::OK;
        }

        $this->stdout("Revert the following $count " . ($count === 1 ? 'update' : 'updates') . ":\n", Console::FG_YELLOW);

        foreach ($models as $model) {
            $this->stdout("\t" . $model->version . PHP_EOL);
        }

        $this->stdout(PHP_EOL);

        if ($this->confirm('Revert the above ' . ($count === 1 ? 'update' : 'updates') . '?')) {
            $reverted = 0;

            foreach ($models as $model) {
                $className = (strpos($model->version, '\\') === false ? $this->module->updateNamespace . '\\' : '') . $model->version;
                $update = $this->initUpdate($className);

                if (!$update->run('down')) {
                    $this->stdout("\n$reverted out of $count " . ($reverted === 1 ? 'update' : 'updates') . " reverted.\n", Console::FG_RED);
                    $this->stdout(PHP_EOL . 'Reverting the Update "'. $className . '" failed. Continuing canceled.' . PHP_EOL, Console::FG_RED);
                    return ExitCode::UNSPECIFIED_ERROR;
                }

                $reverted++;
            }

            $this->stdout("\n$reverted " . ($reverted === 1 ? 'update' : 'updates') . " reverted.\n", Console::FG_GREEN);
            $this->stdout("\nDowngraded successfully.\n", Console::FG_GREEN);
        }

        return ExitCode::OK;
    }

    /**
     * Generates new update source PHP code.
     *
     * @param array $params generation parameters, usually following parameters are present:
     *
     *  - name: string update base name
     *  - className: string update class name
     *
     * @return string generated PHP code.
     */
    protected function generateUpdateSourceCode(array $params): string
    {
        return $this->renderFile(Yii::getAlias($this->templateFile), $params);
    }

    /**
     * @param string $namespace
     *
     * @return string
     */
    private function getNamespacePath(string $namespace): string
    {
        return str_replace('/', DIRECTORY_SEPARATOR, Yii::getAlias('@' . str_replace('\\', '/', $namespace)));
    }

    /**
     * Returns the updates that are not applied.
     * @return array
     */
    protected function getNewUpdates(): array
    {
        $applied = array_flip(UpdateModel::find()->select('version')->asArray()->column());
        $updatePaths = [];

        foreach ($this->updateNamespaces as $namespace) {
            $updatePaths[] = [$this->getNamespacePath($namespace), $namespace];
        }

        $updates = [];

        foreach ($updatePaths as [$updatePath, $namespace]) {
            if (!file_exists($updatePath)) {
                continue;
            }

            $handle = opendir($updatePath);

            while (($file = readdir($handle)) !== false) {
                if ($file === '.' || $file === '..') {
                    continue;
                }

                $path = $updatePath . DIRECTORY_SEPARATOR . $file;

                if (preg_match('/^(u(\d{6}_?\d{6})\D.*?)\.php$/is', $file, $matches) && is_file($path)) {
                    $class = $namespace . '\\' . $matches[1];
                    $time = str_replace('_', '', $matches[2]);
                    $versionName = str_replace($this->module->updateNamespace . '\\', '', $class);

                    if (!isset($applied[$versionName])) {
                        $updates[$time . '\\' . $class] = $class;
                    }
                }
            }

            closedir($handle);
        }

        ksort($updates);

        return array_values($updates);
    }

    /**
     * Creates a new Update instance.
     *
     * @param string $className
     *
     * @return Update
     * @throws InvalidConfigException
     */
    protected function initUpdate(string $className): Update
    {
        if (class_exists($className) && (($update = Yii::createObject($className)) instanceof Update)) {
            /** @var $update Update */
            $update->name = str_replace($this->module->updateNamespace . '\\', '', $className);
            return $update;
        }

        throw new InvalidCallException('Class not found');
    }
}
