<?php

namespace fafcms\updater\commands;

/**
 * Class MigrateController
 *
 * @package fafcms\updater\commands
 */
class MigrateController extends \yii\console\controllers\MigrateController
{
    public const DIRECTION_UP   = 'up';
    public const DIRECTION_DOWN = 'down';

    private bool $isUpdate = false;

    /**
     * {@inheritdoc}
     */
    public $templateFile = '@fafcms/updater/views/createMigrationFile.php';

    /**
     * Runs a single migration file
     *
     * @param string $className Class name of migration file.
     * @param string $direction Migration direction. Can be down or up.
     * @param bool   $isUpdate
     *
     * @return bool
     */
    public function actionRun(string $className, $direction = self::DIRECTION_UP, bool $isUpdate = false): bool
    {
        if ($isUpdate === true) {
            $this->isUpdate = true;
        }

        if ($direction === self::DIRECTION_DOWN) {
            return $this->migrateDown($className);
        }

        return $this->migrateUp($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function addMigrationHistory($version): void
    {
        if (!$this->isUpdate) {
            parent::addMigrationHistory($version);
        }
    }
}
