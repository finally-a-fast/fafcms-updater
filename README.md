[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Readme | Updater
================================================

[![Latest Stable Version](https://img.shields.io/packagist/v/finally-a-fast/fafcms-updater?label=stable&style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-updater)
[![Latest Version](https://img.shields.io/packagist/v/finally-a-fast/fafcms-updater?include_prereleases&label=unstable&style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-updater)
[![PHP Version](https://img.shields.io/packagist/php-v/finally-a-fast/fafcms-updater/dev-master?style=flat-square)](https://www.php.net/downloads.php)
[![License](https://img.shields.io/packagist/l/finally-a-fast/fafcms-updater?style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-updater)
[![Total Downloads](https://img.shields.io/packagist/dt/finally-a-fast/fafcms-updater?style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-updater)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat-square)](http://www.yiiframework.com/)


A Yii2 Extension for creating and running update-scripts like a migration.

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require finally-a-fast/fafcms-updater
```
or add
```
"finally-a-fast/fafcms-core": "dev-master"
```
to the require section of your `composer.json` file.

Documentation
------------

To initialise the database table run:

```
./yii migrate --migrationPath=@vendor/finally-a-fast/fafcms-updater/src/migrations
```

To create a new Update Class run:

```
./yii update/create <name>
```

To run the update run:

```
./yii update <number-of-updates>
```

[Full documentation](https://www.finally-a-fast.com/) will be added soon at https://www.finally-a-fast.com/.

License
-------

**fafcms-updater** is released under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
