[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Updater
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Added migration class to check if already a transaction is active @cmoeke
- Extended and replaced default yii migration controller to make it possible to migrate only a single migration file by running `migrate/run app\\migrations\\m1234_5678_migration up|down` @cmoeke
- Added createMigrationFile template to force usage of own migration class @cmoeke
- Added exception output of migrations inside an update. @cmoeke
- Added methods to create and delete indexes and foreign with auto generated names @cmoeke 

### Changed
- Rewritten update controller to make it possible to have multiple update file locations @cmoeke
- Improved update template @cmoeke
- Updated code style to php 7.4 @cmoeke
- Moved Update.php to base/Update.php @cmoeke
- Changed db side timestamp to php DateTime format @cmoeke

### Fixed
- Fixed migration to make it possible to have a table name with prefix @cmoeke
- Bug when migration is executed directly @cmoeke
- Don't save migrations executed in an update in migration table #1 @StefanBrandenburger

### Deprecated
- UpdateInterface.php is no longer needed @cmoeke
- \fafcms\updater\Update is now deprecated and \fafcms\updater\base\Update should be used instead @cmoeke

## [0.1.0] - 2020-09-29
### Added
- CHANGELOG.md @cmoeke fafcms-core#39
- basic doc folder structure @cmoeke fafcms-core/37
- .gitlab-ci.yml for doc an build creation @cmoeke fafcms-core/38

### Changed
- composer.json to use correct dependencies @cmoeke
- LICENSE to LICENSE.md @cmoeke
- PHP dependency to 7.4 @cmoeke
- Changed ci config to handle new build branch @cmoeke

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-updater/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-updater/-/tree/v0.1.0-alpha
